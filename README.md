# README #

Angular.js app example of directives:

1. Create a new module named FeedsterApp, and then attach it to the <body> element in the view.

2. In the controller, there is a property called $scope.posts containing an array of objects with information about each post. Attach the controller to the <div class="posts"> element.

3. Create a directive named feedsterPost:

    1. Make a new file js/directives/feedsterPost.js. In it, create a new directive named feedsterPost.
    2. Use scope to specify that we'll pass information into this directive through an attribute named post
    3. Use templateUrl to tell this directive to use the js/directives/feedsterPost.html file

4. Include this new JavaScript file in index.html as a <script> element.

5. Next, write the directive's template. In the file js/directives/feedsterPost.html, finish the template so it displays a post's details. Looking at the format of the data in $scope.posts, display each post's author name, author avatar, comment image, and comment text. Remember to use ng-src to display images.

6. In the view inside the .post div, use the <feedster-post> directive to display the details of each post:

    1. On the .post div, use ng-repeat to loop through posts.
    2. Pass each item into the <feedster-post> directive's post attribute.

7. View the result by visiting http://localhost:8000 in the browser.

8. Create another directive named plusOne:

    1. Make a new file js/directives/plusOne.js. In it, create a new directive named plusOne.
    2. Set scope to an empty object {}}
    3. Use templateUrl to tell this directive to use the js/directives/plusOne.html file
    4. Add a link option, and set it to this function named like():

    function(scope, element, attrs) {
      scope.like = function() {
        element.toggleClass('btn-like');
      }
    }

    The like() function toggles the class btn-like on the element.

    5. Include this new JavaScript file in index.html as a <script> element.

    6. In the directive's template js/directives/plusOne.html, use ng-click to run the like() function when the <button> is clicked.

    7. In the view inside the .post div, use the <plus-one> directive to display a +1 button for each post. View the result in the browser.



### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact